package me.gserv.forgeless

import org.bukkit.plugin.java.JavaPlugin

class Forgeless : JavaPlugin() {
    override fun onEnable() {
        super.onEnable()

        this.saveDefaultConfig()

        // this.server.pluginManager.registerEvents(<EventListener instance>, this)
    }

    override fun onDisable() {
        super.onDisable()

        // Do any relevant teardown here - try to support live reloading
    }
}
