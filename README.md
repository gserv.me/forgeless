# Forgeless

**Forgeless** is an attempt at providing an open-source, DRM free plugin for adding custom items and blocks to
Minecraft without requiring any client mods. This will require the use of a specially-crafted texture pack.

It is not known whether we will be able to generate the texture pack at runtime or not, but that is the intention.

---

## Project Status

This project is still in the early stages, and it's not ready to be used just yet.

## Requirements

* None just yet

## Building the project

This project makes use of **Gradle**. Open a terminal and run the following commands from the project directory:

* **Windows**: `gradlew shadowJar`
* **Linux/Mac**: `./gradlew shadowJar`

You will require a JDK installed for this - version 8 or later.

Once this task has been run, you will find a JAR file in `build/lib`, named `Forgeless-<version>-all.jar`. This is the
plugin - drop it in your `plugins/` folder to get started.

## Questions & Contributions

Questions, concerns, ideas? [Open an issue!](https://gitlab.com/gserv.me/Forgeless/issues/new) Pull requests are
welcome, once the project has gotten off the ground.
